package observer

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type myObserver struct {
	called     bool
	calledWith interface{}
}

func (m *myObserver) reset() {
	m.called = false
	m.calledWith = nil
}

func (m *myObserver) Update(ctx context.Context, data interface{}) error {
	switch data.(type) {
	case string:
		m.called = true
		m.calledWith = data
	case bool:
		m.called = true
		m.calledWith = data
		time.Sleep(2 * time.Second)
	}
	return ctx.Err()
}

func TestObserverNotify(t *testing.T) {
	obs := New()
	myObs := &myObserver{}
	obs.Observe("test", myObs)
	err := obs.NotifyObservers(context.Background(), "called value")
	assert.Equal(t, nil, err)
	assert.Equal(t, true, myObs.called)
	cwStr, ok := myObs.calledWith.(string)
	assert.Equal(t, true, ok)
	assert.Equal(t, "called value", cwStr)

	myObs.reset()
	obs.Observe("test", nil)
	err = obs.NotifyObservers(context.Background(), "called again")
	assert.Equal(t, nil, err)
	assert.Equal(t, false, myObs.called)
	assert.Equal(t, nil, myObs.calledWith)
}

func TestObserverNotifyAsync(t *testing.T) {
	obs := New()
	myObs := &myObserver{}
	obs.Observe("test", myObs)
	errChan := obs.NotifyObserversAsync(context.Background(), true, 100*time.Millisecond)
	err := <-errChan
	assert.NotEqual(t, nil, err)
	assert.Equal(t, true, myObs.called)
}

func TestObserverSlotNotify(t *testing.T) {
	obs := New()
	calledCount := 0
	obs.Slot("test", func(context.Context, interface{}) error {
		calledCount++
		return nil
	})
	for i := 0; i < 10; i++ {
		err := obs.NotifyObservers(context.Background(), "called value")
		assert.Equal(t, nil, err)
		assert.Equal(t, i+1, calledCount)
	}
	obs.Slot("test", nil)
	for i := 0; i < 10; i++ {
		err := obs.NotifyObservers(context.Background(), "called value")
		assert.Equal(t, nil, err)
		assert.Equal(t, 10, calledCount)
	}
}
