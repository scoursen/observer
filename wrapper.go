package observer

import "context"

type wrapper struct {
	notifyFunc NotifyFunc
}

func (w *wrapper) Update(ctx context.Context, data interface{}) error {
	return w.notifyFunc(ctx, data)
}

func NewWrapper(notifyFunc NotifyFunc) Observer {
	return &wrapper{notifyFunc: notifyFunc}
}
