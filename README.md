Simple go library to add observer/observable type functionality

```go
obs := observer.New()
obs.Observe("example", observer.NewWrapper(func(_ context.Context, data interface{})) {
    fmt.Println("Received:", data)
})
obs.NotifyObservers("hello world")
```
