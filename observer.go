package observer

import (
	"context"
	"strings"
	"sync"
	"time"

	"golang.org/x/sync/errgroup"
)

type errorSlice []error
type NotifyFunc func(context.Context, interface{}) error

func (e errorSlice) Error() string {
	strs := make([]string, len(e))
	for i, err := range e {
		strs[i] = err.Error()
	}
	return strings.Join(strs, "\n")
}

type Observable interface {
	Observe(string, Observer)
	Slot(string, NotifyFunc)
	NotifyObservers(context.Context, interface{}) error
	NotifyObserversAsync(context.Context, interface{}, time.Duration) <-chan error
}

type Observer interface {
	Update(context.Context, interface{}) error
}

type BasicObserved struct {
	lock      sync.RWMutex
	observers map[string]Observer
}

func (o *BasicObserved) checkInit() {
	o.lock.Lock()
	defer o.lock.Unlock()
	if o.observers == nil {
		o.observers = make(map[string]Observer)
	}
}

func (o *BasicObserved) Observe(name string, obs Observer) {
	/*
		Attach an `Observer` to this struct, assigning it to a unique name.
		If there already is an `Observer` with this name, the argument `obs` will replace it.
		To stop observing a struct, call this function with `nil` as the `obs` argument.
	*/
	o.checkInit()
	o.lock.Lock()
	defer o.lock.Unlock()
	if obs == nil {
		_, found := o.observers[name]
		if found {
			delete(o.observers, name)
		}
	} else {
		o.observers[name] = obs
	}
}

func (o *BasicObserved) Slot(name string, notifyFunc NotifyFunc) {
	if notifyFunc == nil {
		o.Observe(name, nil)
	} else {
		wrap := NewWrapper(notifyFunc)
		o.Observe(name, wrap)
	}
}

func (o *BasicObserved) NotifyObservers(ctx context.Context, data interface{}) error {
	observers := func() []Observer {
		o.checkInit()
		o.lock.RLock()
		defer o.lock.RUnlock()
		observers := make([]Observer, 0)
		for _, v := range o.observers {
			observers = append(observers, v)
		}
		return observers
	}()
	errs := make([]error, 0)
	for _, obs := range observers {
		if err := obs.Update(ctx, data); err != nil {
			errs = append(errs, err)
		}
	}
	if len(errs) == 0 {
		return nil
	}
	return errorSlice(errs)
}

func (o *BasicObserved) NotifyObserversAsync(ctx context.Context, data interface{}, timeout time.Duration) <-chan error {
	/*
		Pass a negative timeout value (such as -1) to disable the timeout
	*/
	observers := func() []Observer {
		o.checkInit()
		o.lock.RLock()
		defer o.lock.RUnlock()
		observers := make([]Observer, 0)
		for _, v := range o.observers {
			observers = append(observers, v)
		}
		return observers
	}()
	var cf func()
	var asyncCtx context.Context
	if timeout > 0 {
		asyncCtx, cf = context.WithTimeout(ctx, timeout)
	} else {
		asyncCtx = ctx
		cf = func() {}
	}
	eg, egCtx := errgroup.WithContext(asyncCtx)
	errChan := make(chan error)
	go func() {
		defer cf()
		defer close(errChan)
		for _, obs := range observers {
			eg.Go(func() error {
				return obs.Update(egCtx, data)
			})
		}
		err := eg.Wait()
		select {
		case errChan <- err:
		case <-egCtx.Done():
		}
	}()
	return errChan
}

func New() *BasicObserved {
	return &BasicObserved{
		observers: make(map[string]Observer),
	}
}
